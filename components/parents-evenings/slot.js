import React from "react";
import { Menu, Transition } from "@headlessui/react";
import Portal from "@reach/portal";
import { TrashIcon } from "components/icons";

const Slot = ({ children }) => {
  return (
    <div className="relative">
      <Menu>
        {({ open }) => (
          <>
            <span className="rounded-md shadow">
              <Menu.Button className="inline-flex justify-center w-full px-4 py-2 text-sm font-medium leading-5 text-gray-700 transition duration-150 ease-in-out bg-white border border-gray-300 rounded-md hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-50 active:text-gray-800">
                slot
              </Menu.Button>
            </span>
            <Transition
              show={open}
              enter="transition ease-out duration-100"
              enterFrom="transform opacity-0 scale-95"
              enterTo="transform opacity-100 scale-100"
              leave="transition ease-in duration-75"
              leaveFrom="transform opacity-100 scale-100"
              leaveTo="transform opacity-0 scale-95"
              className="absolute z-10"
            >
              <Menu.Items
                static
                className="w-56 mt-2 origin-top-right bg-white border border-gray-200 divide-y divide-gray-100 rounded-md shadow-lg outline-none"
              >
                <div className="py-1">
                  <Menu.Item>
                    {({ active }) => (
                      <a
                        href="#addHeader"
                        className={`${
                          active ? "bg-gray-100 text-gray-900" : "text-gray-700"
                        } flex items-center w-full px-4 py-2 text-sm leading-5 text-left`}
                        onClick={e => {
                          e.preventDefault();
                          // add header before
                        }}
                      >
                        Add header before
                      </a>
                    )}
                  </Menu.Item>
                  <Menu.Item>
                    {({ active }) => (
                      <a
                        href="#makeBreak"
                        className={`${
                          active ? "bg-gray-100 text-gray-900" : "text-gray-700"
                        } flex items-center w-full px-4 py-2 text-sm leading-5 text-left`}
                        onClick={e => {
                          e.preventDefault();
                          // change to break
                        }}
                      >
                        <span>Change to Break</span>
                      </a>
                    )}
                  </Menu.Item>
                  <Menu.Item>
                    {({ active }) => (
                      <a
                        href="#mergeNext"
                        className={`${
                          active ? "bg-gray-100 text-gray-900" : "text-gray-700"
                        } flex items-center w-full px-4 py-2 text-sm leading-5 text-left`}
                        onClick={e => {
                          e.preventDefault();
                          // merge next slot
                        }}
                      >
                        <span>Merge with next slot</span>
                      </a>
                    )}
                  </Menu.Item>
                  <Menu.Item>
                    {({ active }) => (
                      <a
                        href="#mergePrevious"
                        className={`${
                          active ? "bg-gray-100 text-gray-900" : "text-gray-700"
                        } flex items-center w-full px-4 py-2 text-sm leading-5 text-left`}
                        onClick={e => {
                          e.preventDefault();
                          // merge with previous slot
                        }}
                      >
                        <span>Merge with previous slot</span>
                      </a>
                    )}
                  </Menu.Item>
                  <div className="border-t border-gray-100 my-1" />
                  <Menu.Item>
                    {({ active }) => (
                      <a
                        href="#delete"
                        className={`${
                          active ? "bg-gray-100 text-gray-900" : "text-gray-700"
                        } flex items-center w-full px-4 py-2 text-sm leading-5 text-left`}
                        onClick={e => {
                          e.preventDefault();
                          // remove the slot
                        }}
                      >
                        <TrashIcon className="w-5 h-5 mr-2 text-red-400" />
                        <span>Delete</span>
                      </a>
                    )}
                  </Menu.Item>
                </div>
              </Menu.Items>
            </Transition>
          </>
        )}
      </Menu>
    </div>
  );
};

export default Slot;
