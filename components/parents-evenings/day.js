import React, { useState, forwardRef } from "react";
import PropTypes from "prop-types";
import { Button, Card } from "components/elements";
import Slot from "./slot";
import { TrashIcon } from "components/icons";

const Day = forwardRef((props, ref) => {
  return (
    <Card className="p-8 space-y-4">
      <div className="grid grid-cols-4 md:flex gap-4 items-end">
        <div className="space-y-2">
          <label htmlFor="date" className="block">
            Start Time & Date
          </label>
          <input className="form-input" placeholder="Date" name="date" />
        </div>
        <div className="space-y-2">
          <label htmlFor="slotCount" className="block">
            Slot Count
          </label>
          <input className="form-input" placeholder="Slots" name="slotCount" />
        </div>
        <div className="space-y-2">
          <label htmlFor="duration" className="label">
            Slot Duration (mins)
          </label>
          <div className="grid gap-2 grid-cols-6 grid-auto-col">
            {[5, 10, 15, 20, 25, 30].map(time => (
              <span key={time}>
                <Button
                  size="md"
                  key={time}
                  color={time === 15 ? "blue" : "white"}
                  onClick={() => {
                    // on click action
                  }}
                  className="w-full justify-center"
                  spanClass="w-full"
                >
                  {time}
                </Button>
              </span>
            ))}
          </div>
        </div>
        <div className="">
          <Button>Create Slots</Button>
        </div>
        <div className="ml-auto">
          <Button color="white" size="sm">
            <TrashIcon className="w-5 h-5 text-red-500" />
          </Button>
        </div>
      </div>
      <div className="flex flex-wrap gap-x-2 gap-y-4 py-4">
        <Slot />
        <Slot />
        <Slot />
        <Slot />
        <Slot />
        <Slot />
        <Slot />
        <Slot />
      </div>
    </Card>
  );
});

Day.propTypes = {
  // to be added
};

export default Day;
