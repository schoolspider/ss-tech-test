# School Spider Tech Test

This project is set up using [Next.js](https://nextjs.org) and [TailwindCSS](https://tailwindcss.com)

What we are looking for is for you to set up the base day and slot manager to see how you handle managing state.

A parents evening can occur on multiple days and a day can have multiple timeslots in which parents can book onto.

You can see the design in the design document in the root of the repo.
We have provided some pre designed components so you dont have to get bogged down with that we are focusing on functionality.

## Getting Started

Clone this repo and then execute the following commands

```bash
npm install && npm run dev
# or
yarn install && yarn dev
```

# What is required
We would like you to create the following functionality for the test:

1. Choose start time of day, choose duration of slots, choose number of slots to create. Create slots for the day based on this
2. Ability to change the start time of day and all slots in that day update to correct time
3. Click on a slot and delete. 
4. Click on slot to add a break. Break length will default to duration of slot. Duration of break can be changed and time slots after this should be updated correctly.
5. Ability to add multiple days with same functionality
